\name{sayhello}
\alias{sayhello}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{helloworld2
%%  ~~function to do ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
sayhello(x)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{no areguments!}{
%%     ~~Describe \code{x} here~~
}
}
\details{
just prints "hello world"
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
Rstudio!
devtool
https://seams-workshop.gitlab.io/practical/reuse/
http://web.mit.edu/insong/www/pdf/rpackage_instructions.pdf
%% ~put references to the literature/web site here ~
}
\author{ Jeremy!
%%  ~~who you are~~
}
\note{ this is a function created as part of a practice tutorial
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.
## THIS PACKAGE JUST HAS ONE FUNCTION:
helloworld2()
## The function is currently defined as
helloworld2 
{print("hello world!!!")
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }% use one of  RShowDoc("KEYWORDS")
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
